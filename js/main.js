/*  ---  Vanilla  --- */

/* Check if element has specific class */
function hasClass(element, className) {
	return (' ' + element.className + ' ').indexOf(' ' + className + ' ') > -1;
};

/* Calculate difference */
function calculateDifference(firstValue, secondValue){
	if (firstValue > secondValue) {
		return firstValue-secondValue
	} else {
		return secondValue-firstValue
	}
}

/* Mobile navigation behavior */
var mobileNavTrigger = document.getElementById('mobile-menu-trigger');
var mobileNavTriggerContent = mobileNavTrigger.getElementsByTagName('span')[0];
var navigationDiv = document.getElementsByClassName('navigation')[0];
var navigationDivUrl = navigationDiv.getElementsByTagName('ul')[0];
mobileNavTrigger.onclick = function(){
	if (hasClass(navigationDiv, 'show')) {
		navigationDiv.classList.remove('show');
		mobileNavTriggerContent.className = 'fa fa-bars';
	} else {
		navigationDiv.classList.add('show');
		mobileNavTriggerContent.className = 'fa fa-times';
	}
}
navigationDivUrl.onclick = function(){
	navigationDiv.classList.remove('show');
	mobileNavTriggerContent.className = 'fa fa-bars';
}

/* --- jQuery --- */

$(window).scroll(function() {

	$('.white-background').each(function() {
		topOfWindow = $(window).scrollTop();
		elementTop = $(this).offset().top - 20;
		elementBottom = $(this).outerHeight() + elementTop;
		if (topOfWindow > elementTop && topOfWindow < elementBottom) {
			$('header').addClass('dark-header');
			$('.svg-logo').attr('src', 'media/jagodka_color.svg');
			return false;
		} else {
			$('header').removeClass('dark-header');
			$('.svg-logo').attr('src', 'media/jagodka.svg')
		}
	});

	$('#section-2').each(function(){
		var elementPosition = $(this).offset().top;
		var topOfWindow = $(window).scrollTop();
		if (elementPosition < topOfWindow+300) {
			$(this).addClass('a-slide-up');
		}
	});

	$('#section-6').each(function(){
		var elementPosition = $(this).offset().top;
		var topOfWindow = $(window).scrollTop();
		if (elementPosition < topOfWindow+300) {
			$(this).addClass('bg-effect');
			$('.s6-1').css({'animation':'slide-right-50 1s ease','animation-fill-mode':'forwards','-webkit-animation-fill-mode':'forwards','animation-delay':'1s','-webkit-animation-delay':'1s'});
			$('.s6-2').css({'animation':'slide-left-50 1s ease','animation-fill-mode':'forwards','-webkit-animation-fill-mode':'forwards','animation-delay':'1s','-webkit-animation-delay':'1s'});
		}
	});

	$('.day').each(function(){
		var elementPosition = $(this).offset().top;
		var topOfWindow = $(window).scrollTop();
		if (elementPosition < topOfWindow+700) {
			$(this).addClass('a-popout');
		}
	});
	$('.s5').each(function(){ 
		var elementPosition = $(this).offset().top;
		var topOfWindow = $(window).scrollTop();
		if (elementPosition < topOfWindow+700) {
			$(this).css({'animation':'slide-right-50 1s ease','opacity':'1'});
		}
	});
});

$('.day').click(function(){
	var date = $(this).data('day');
	var title = $(this).data('day-title');
	var desc = $(this).data('day-description');
	$('#day-box').text('');
	$('#day-title-box').text('');
	$('#day-description-box').text('');
	$('#day-box').append(date + '.2016');
	$('#day-title-box').append(title);
	$('#day-description-box').append(desc);
});

$('.filter').click(function(){
	$('a.filter').removeClass("active");
	var group = $(this).data('group');
	if (group === "maluszki") { 
		$('.m').addClass('active');
		$('.s4 li.starszaki, .s4 li.sredniaki').addClass('a-fade-out');
	} else if (group === "sredniaki") {
		$('.sr').addClass('active');
		$('.s4 li.sredniaki').removeClass("a-fade-out");
		$('.s4 li.starszaki').addClass('a-fade-out');
	} else if (group === "starszaki") {
		$('.st').addClass('active');
		$('.s4 li.starszaki, .s4 li.sredniaki').removeClass("a-fade-out");
	}
});